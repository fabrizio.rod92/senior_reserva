import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:sinpe_movil/splash_page.dart';
import 'package:sinpe_movil/src/pages/pase_dinero.dart';
import 'package:sinpe_movil/src/routes/routes.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return PlatformApp(
      debugShowCheckedModeBanner: false,
      title: 'Sinpe Móvil',
      initialRoute: '/',
      routes: getApplicationRoutes(),
      onGenerateRoute: (settings){
        return platformPageRoute(
          builder: (BuildContext context) => PaseDineroPage()
        );
      },
    );
  }
}