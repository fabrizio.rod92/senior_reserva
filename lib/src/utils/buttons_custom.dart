import 'package:flutter/material.dart';

class ButtonCustom extends StatelessWidget {
  String txt;
  Color color;
  GestureTapCallback ontap;
  double heigth;

  ButtonCustom({this.txt, this.color, this.ontap, this.heigth});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: ontap,
      child: Container(
        height: heigth,
        width: 300.0,
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.all(
              const Radius.circular(30.0)
          ),
        ),
        child: Center(
            child: Text(
              txt,
              style: TextStyle(
                color: Colors.white,
              ),
            )),
      ),
    );
  }
}