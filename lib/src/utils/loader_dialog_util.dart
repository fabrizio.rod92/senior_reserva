import 'package:flutter/material.dart';


class LoaderDialog extends StatelessWidget {
  

  @override
  Widget build(BuildContext context) => SimpleDialog(
    children: <Widget>[
      Container(
        color: Colors.white,
        height: 200,
        width: 400,
        child: Image.asset('assets/loader/slidin_squares.gif'),
        ),      
    ],
  );
}