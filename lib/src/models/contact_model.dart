import 'dart:convert';

Contacto clientFromJson(String str) {
    final jsonData = json.decode(str);
    return Contacto.fromJson(jsonData);
}

String clientToJson(Contacto data) {
    final dyn = data.toJson();
    return json.encode(dyn);
}

class Contacto {
    int id;
    String nombre;
    String telefono;

    Contacto({
        this.id,
        this.nombre,
        this.telefono,
    });

    factory Contacto.fromJson(Map<String, dynamic> json) => new Contacto(
        id: json["id"],
        nombre: json["nombre"],
        telefono: json["telefono"]
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "nombre": nombre,
        "telefono ": telefono,
    };
}