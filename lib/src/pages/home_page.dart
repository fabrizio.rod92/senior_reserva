import 'dart:ui' as prefix0;

import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';

import 'package:sinpe_movil/src/providers/menu_provider.dart';
import 'package:sinpe_movil/src/utils/icono_string_util.dart';
  
class HomePage extends StatelessWidget {

  final opciones = ['Uno', 'Dos', 'Tres', 'Cuatro','Cinco'];
  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(      
      appBar: PlatformAppBar(        
        title: Text('Sinpe Móvil'),
        android: (_) => MaterialAppBarData(),
        ios: (_)=> CupertinoNavigationBarData(),
      ),
      body: _lista(),
        // ListView(
        // padding: EdgeInsets.all(20.0),  
        // children: <Widget>[
        //   _lista()
        // ],     
        // children: <Widget>[
        //     cardTipo1(),
        //     SizedBox(height:20.0),
        //     cardTipo2()
        // ],
    
    );
  }

  Widget _lista() {    

      return FutureBuilder(
          future: menuProvider.cargarData(),
          initialData: [],
          builder: (context, AsyncSnapshot<List<dynamic>> snapshot){
            
            return ListView(
                padding: EdgeInsets.all(20.0),
                children: _cardTipo2(snapshot.data, context),
            );
          },
      );
  }

  List<Widget> _cardTipo2(List<dynamic> data, BuildContext context) {
    final List<Widget> widgets = [];

    data.forEach((opt){
    final card = InkWell(
      child: Container(
        child: Column(
          children: <Widget>[
            Image(
              height: 90.0,
              width: 400.0,
              image: AssetImage(opt['imagen']),
              fit: BoxFit.fill,
            ),
            Container(
              padding: EdgeInsets.all(10.0),
              child: Text(opt['texto'])
            )
          ],
        ),
    ),
    onTap: (){
        Navigator.pushNamed(context, opt['ruta']);
    },
    );

    final container = Container(                      
        decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20.0),
        boxShadow: <BoxShadow>[
          BoxShadow(
            blurRadius: 10.0,
            color: Colors.black26,
            spreadRadius: 2.0,
            offset: Offset(2.0,10.0)
          )
        ],
      ),
      child: ClipRRect(
          child: card,
          borderRadius: BorderRadius.circular(20.0),
        )
    );

    widgets..add(container)
           ..add(SizedBox(height: 20.0,));

  });

  return widgets;

  }



  List<Widget>  _listItems(List<dynamic> data, BuildContext context) {
    
    final List<Widget> opciones = [];

    data.forEach((opt){

      final widgetTemp = ListTile(
        title: Text(opt['texto']),
        leading: getIcon(opt['icon']),
        trailing: Icon(Icons.keyboard_arrow_right,color: Colors.blue),
        onTap:(){
         
          Navigator.pushNamed(context, opt['ruta']);
        } ,
      );

      opciones..add(widgetTemp)
              ..add(Divider());

    });

    return opciones;
  }

  Widget cardTipo1(){

      return Card(
        elevation: 10.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0)
        ),
        child: Column(
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.info_outline),
              title: Text('Cualquier cosa veo que pasa'),
              subtitle: Text('Aquí escribo el cuerpo de lo que lleva esta card'),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                FlatButton(
                  child: Text('Cancelar'),
                  onPressed: (){}
                ),
                FlatButton(
                  child: Text('Ok'),
                  onPressed: (){}
                )
              ],
            )
          ],
        ),
      );
  }

}