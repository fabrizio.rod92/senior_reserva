import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:sinpe_movil/src/utils/buttons_custom.dart';
import 'package:sinpe_movil/src/utils/contacts_dialog.dart';
import 'package:sinpe_movil/src/utils/loader_dialog_util.dart';
import 'package:sinpe_movil/src/utils/send_message_util.dart';


class PaseDineroPage extends StatefulWidget{
  @override
  _PaseDineroPage createState() => _PaseDineroPage();
}


class _PaseDineroPage extends State<PaseDineroPage> {
  final String _codigoPais = "+506";
  String _telefono = '';
  String _monto = '';
  String _concepto = '';


  //Contacts
  Iterable<Contact> _contacts;
  Contact _actualContact;
  ProgressDialog pr;

  TextEditingController phoneTextFieldController = TextEditingController();
  TextEditingController guestnameTextFieldController = TextEditingController();

  final _formPaseDinero = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {

    pr = new ProgressDialog(context);

    return Form(
        key: _formPaseDinero,
        child: Scaffold(          
          appBar: AppBar(
            title: Text('Pase Dinero'),
          ),
          body: ListView(
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
            children: <Widget>[
              _crearInputTelefono(),
              Padding(padding: EdgeInsets.only(top: 30.0)),             
              _crearInputMonto(),
              Padding(padding: EdgeInsets.only(top: 30.0)), 
              _crearInputConcepto(),
              Padding(padding: EdgeInsets.only(top: 30.0)),   
              ButtonCustom(
                color: Color(0xFF4458be),
                heigth: 50.0,
                txt: "Enviar",
                ontap: () {
                  if(_formPaseDinero.currentState.validate()){
                    _enviarMensaje();
                  }          
                },
              ),  
                     
            ],
          ),         
          bottomNavigationBar: BottomAppBar(
            child: Container(
                height: 50.0,
              ),
          ),
          floatingActionButton: _crearBotonesFlotantes(),         
          floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      ),
    );
  }

  Widget _crearInputTelefono() {
    return 
        Container(
            width: MediaQuery.of(context).size.width*1,
            padding:
            EdgeInsets.only(left: 10.0, right: 10.0, top: 0.0, bottom: 0.0),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                color: Colors.white,
                boxShadow: [BoxShadow(blurRadius: 10.0, color: Colors.black12)]),
            child: TextFormField(

              controller: phoneTextFieldController,
              validator: (val) {
                if(val == ''){
                  return 'Por favor ingrese un número';
                }
                else if(val.length != 8){
                  return 'La longitud del número de telefono debe ser 8.';
                }
                return null;
              },
              decoration: new InputDecoration(
                  border: InputBorder.none,
                  icon: Icon(Icons.phone),
                  labelText: 'Número teléfono',
                  prefixText: '     ',
                  suffixStyle: const TextStyle(color: Colors.green)
              ),
              onChanged: (value){
          
                setState(() {
                  _telefono = value; 
                });
              },
            )
        );       
  }

  Widget _crearInputMonto() {
    
      return Container(
        width: MediaQuery.of(context).size.width*1,
        padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 0.0, bottom: 0.0),
        decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20.0),
        color: Colors.white,
        boxShadow: [BoxShadow(blurRadius: 10.0, color: Colors.black12)]),
        child: TextFormField(
        keyboardType: TextInputType.number ,
        inputFormatters: <TextInputFormatter>[
           WhitelistingTextInputFormatter.digitsOnly
        ],
        textCapitalization: TextCapitalization.sentences,
        decoration: InputDecoration(
          border: InputBorder.none,
          labelText: 'Monto',
          hintText: 'Digita el monto a transferir',
          icon: Icon(Icons.attach_money)
        ),
        onChanged: (value){
          
          setState(() {
            _monto = value; 
          });
        },
        validator: (value) {
          if(value == ''){
            return 'Debe ingresar un monto.';
          }
          else if ( int.parse(value) == 0) {
            return 'Debe ser un monto mayor a 0.';
          }
          return null;
        },
    ),
      );
  }

  Widget _crearInputConcepto() {
      return Container(
        width: MediaQuery.of(context).size.width*1,
        padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 0.0, bottom: 0.0),
        decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20.0),
        color: Colors.white,
        boxShadow: [BoxShadow(blurRadius: 10.0, color: Colors.black12)]),
        child: TextFormField(
        textCapitalization: TextCapitalization.sentences,
        decoration: InputDecoration(
          border: InputBorder.none,
          labelText: 'Descripción',
          hintText: 'Digita la descripción',
          icon: Icon(Icons.message)
        ),
        onChanged: (value){
          
          setState(() {
            _concepto = value; 
          });
        },
        validator: (value) {
          if (value.isEmpty) {
            return 'Debe incluir el concepto para pasar dinero.';
          }
          return null;
        },
    ),
      );
  }

  void _enviarMensaje(){
     String message = "PASE " + _monto+" "+ _telefono +" "+_concepto;

    sendSMS(message);
    Navigator.pop(context);
  }
  
  // Getting list of contacts from AGENDA
  refreshContacts() async {
    PermissionStatus permissionStatus = await _getContactPermission();
    if (permissionStatus == PermissionStatus.granted) {
      var contacts = await ContactsService.getContacts();
      setState(() {
        _contacts = contacts;
      });
    } else {
      _handleInvalidPermissions(permissionStatus);
    }
  }

  // Asking Contact permissions
  Future<PermissionStatus> _getContactPermission() async {
    PermissionStatus permission = await PermissionHandler().checkPermissionStatus(PermissionGroup.contacts);
    if (permission != PermissionStatus.granted && permission != PermissionStatus.disabled) {
      Map<PermissionGroup, PermissionStatus> permissionStatus = await PermissionHandler().requestPermissions([PermissionGroup.contacts]);
      return permissionStatus[PermissionGroup.contacts] ?? PermissionStatus.unknown;
    } else {
      return permission;
    }
  }

  // Managing error when you don't have permissions
  void _handleInvalidPermissions(PermissionStatus permissionStatus) {
    if (permissionStatus == PermissionStatus.denied) {
      throw new PlatformException(
          code: "PERMISSION_DENIED",
          message: "Access to location data denied",
          details: null);
    } else if (permissionStatus == PermissionStatus.disabled) {
      throw new PlatformException(
          code: "PERMISSION_DISABLED",
          message: "Location data is not available on device",
          details: null);
    }
  }

  // Showing contact list.
  Future<Null> _showContactList(BuildContext context) async {

    List<Contact> favoriteElements = [];
    final InputDecoration searchDecoration = const InputDecoration();
    
    pr.show();
    
    await refreshContacts();
    if (_contacts != null)
    {
     
      pr.dismiss();
      showDialog(
        context: context,
        builder: (_) =>
            SelectionDialogContacts(
              _contacts.toList(),
              favoriteElements,
              showCountryOnly: false,
              emptySearchBuilder: null,
              searchDecoration: searchDecoration,
            ),
      ).then((e) {
        if (e != null) {
          setState(() {
            _actualContact = e;
          });
          _telefono = '';
          phoneTextFieldController.text = '';
          guestnameTextFieldController.text = _actualContact.middleName;
          _telefono = _actualContact.phones.last.value;
          var finIndex = _codigoPais.length;

          if(_codigoPais == _telefono.substring(0,finIndex)){
            _telefono = _telefono.substring(finIndex, _telefono.length);
          }
          phoneTextFieldController.text = _telefono;

        }
      });


    }
  }

  Widget _crearBotonesFlotantes() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        FloatingActionButton(
                    heroTag: "btnContactos",
                    onPressed: (){
                      _showContactList(context);
                    },            
                    child: Icon(Icons.contacts),
        ),
         FloatingActionButton(
                    heroTag: "btnFavoritos",
                    onPressed: (){},            
                    child: Icon(Icons.favorite),
        ),
      ],
    );
  }
}