import 'package:flutter/material.dart';
import 'package:sinpe_movil/src/utils/send_message_util.dart';
  
class SaldoPage extends StatelessWidget {

  SaldoPage(BuildContext context){
    String message = "SALDO";

    sendSMS(message);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold();
  }
}